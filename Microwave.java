public class Microwave{
   private String food;
   private int temperature;
   private int time;
   private boolean lock;
   
   public Microwave(String food, int temperature, int time){
      this.food = food;
      this.temperature = temperature;
      this.time =time;
   }

   public void setFood(String food){
      this.food = food;
   }
   
   public void setTime(int time){
      this.time = time;
   }

   public void setTemperature(int temperature){
	   this.temperature = temperature;
   }

   public String getFood(){
      return this.food;
   }

   public int getTemperature(){
      return this.temperature;
   }

   public int getTime(){
      return this.time;
   }

   public boolean getLock(){
      return this.lock;
   }

   public int heat(){
      // For each second in the microwave, 
      // food temperature increasing by 3 degrees 
      int finalTemperature = time * 3 + temperature;
      return finalTemperature;
   }
   public int freeze(){
      // For each second in the microwave, 
      // food temperature decreasing by 3 degrees 
      int finalTemperature = temperature - time * 3; 
      if (finalTemperature < 0){
         return 0;    
      }	
      return finalTemperature;
   }

   public void changeDoorState(String state){
      if (state.equals("open")){
         openLock();
      }
      else if (state.equals("close")){
         closeLock();
      }
      else{
         System.out.println("Invalid Input!");
      }
   }

   private void openLock(){
      this.lock = true;
	  System.out.println("Microwave door is open");
   }
   private void closeLock(){
      this.lock = false;
	  System.out.println("Microwave door is closed");
   }
}
