import java.util.Scanner;

public class ApplianceStore{
   public static void main(String[] args){
      Scanner scan = new Scanner(System.in);
      Microwave[] applianceArray = new Microwave[4];
         for (int i = 0; i < applianceArray.length; i++){

            System.out.println("What dish you want to microwave?");
            String food = scan.nextLine();

            System.out.println("What is your dish initial temperature?");
            int temperature = scan.nextInt();

            System.out.println("For how long you want to microwave your food?");
            System.out.println("(Your answer will be converted in seconds)");
	        int time = scan.nextInt();

	        applianceArray[i] = new Microwave(food, temperature, time);

            scan.nextLine();
    }
	 
      System.out.println();

      Microwave lastMicrowave = applianceArray[applianceArray.length-1];
      System.out.println("Food inside the last microwave is " + lastMicrowave.getFood());
      System.out.println("Intial temperature of the dish is  " + lastMicrowave.getTemperature());
      System.out.println("Time to microwave is " + lastMicrowave.getTime() + " seconds");
      
      System.out.println();

      Microwave firstMicrowave = applianceArray[0];
      System.out.println("Your food is heated up to " + firstMicrowave.heat() + " degrees celsius");
      System.out.println("Your food temperature is lowered to " + firstMicrowave.freeze() + " degrees celsius");

	  System.out.println();
	  
	  //Lab 4:
      applianceArray[1].changeDoorState("open");
      applianceArray[1].changeDoorState("close");
	  
	  System.out.println();
	  
	  System.out.println("The first field of last appliance is: " + applianceArray[applianceArray.length-1].getFood());
	  System.out.println("The second field of last appliance is: " + applianceArray[applianceArray.length-1].getTemperature());
	  System.out.println("The third field of last appliance is: " + applianceArray[applianceArray.length-1].getTime());
	  
	  System.out.println();
	  
	  System.out.println("Set the food of the last microwave: ");
	  String food = scan.nextLine();
	  applianceArray[applianceArray.length-1].setFood(food);
	  
	  System.out.println("Set the temperature of the last microwave: ");
	  int temperature = scan.nextInt();
	  applianceArray[applianceArray.length-1].setTemperature(temperature);
	  
	  System.out.println("Set the time of the last microwave: ");
	  int time = scan.nextInt();
	  applianceArray[applianceArray.length-1].setTime(time);
	  
	  
	  System.out.println();
	  
	  System.out.println("The first field of last appliance is: " + applianceArray[applianceArray.length-1].getFood());
	  System.out.println("The second field of last appliance is: " + applianceArray[applianceArray.length-1].getTemperature());
	  System.out.println("The third field of last appliance is: " + applianceArray[applianceArray.length-1].getTime());
   }
}
